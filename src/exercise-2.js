const getTheExactNumber = (array) => {
  // Filter out the multiples of 3, and find the maximum among them
  const multiplesOfThree = array.filter(number => number % 3 === 0);
  return Math.max(...multiplesOfThree);
};

export default getTheExactNumber;
