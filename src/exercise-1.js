const getCommonItems = (array1, array2) => {
  // Use filter() to filter out the common elements from both arrays
  return array1.filter(item => array2.includes(item));
};

export default getCommonItems;